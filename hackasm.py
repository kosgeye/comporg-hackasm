# A Hack assembler in 5 Minutes
import re
import sys
 
# Set up hash tables (dictionaries, in Python) to capture all of
# the instruction transformations. This is a "lookup table," of sorts.
# When we encounter an "A", it will always be transformed to
# "110000", which we will get by looking up the key "A" in the dictionary
# called "comp".
#
# I like my hash tables to line up all pretty-like when I can pull it off.

input_file = sys.argv[1]
output_file = sys.argv[2] 


comp0 = {'A'  : '110000',
        'D'   : '001100',
        'D+A' : '000010',
        '0'   : '101010',
        '1'   : '111111',
        '-1'  : '111010',
        '!D'  : '001101',
        '!A'  : '110001',
        '-D'  : '001111',
        '-A'  : '110011',
        'D+1' : '011111',
        'A+1' : '110111',
        'D-1' : '001110',
        'A-1' : '110010',
        'D+A' : '000010',
        'D-A' : '010011',
        'A-D' : '000111',
        'D&A' : '000000',
        'D|A' : '010101'}

comp1 = {'M'  : '110000',
        'D+M' : '000010',
        '-M'  : '110011',
        '!M'  : '110001',
        'M+1' : '110111',
        'M-1' : '110010',
        'D+M' : '000010',
        'D-M' : '010011',
        'M-D' : '000111',
        'D&M' : '000000',
        'D|M' : '010101'}
      
# Do the same for the destinations
dest = {'D'   : '010',
        'M'   : '001',
        'null'  : '000',
        'MD'  : '011',
        'A'   : '100',
        'AM'  : '101',
        'AD'  : '110',
        'AMD' : '111'}
j_bits = {'null'  : '000',
          'JGT'   : '001',
          'JEQ'   : '010',
          'JGE'   : '011',
          'JLT'   : '100',
          'JLE'   : '110',
          'JNE'   : '101',
          'JMP'   : '111'}
variables = {}
 
# We will need a table for the jumps.
 
# Open the file. It might be good to read it into an
# array; at least, for small (lines < 100K) programs.
f = open(input_file, 'r')
 
# Convert a number into a 15 bit binary number.
# From this Stack Overflow post:
# http://goo.gl/ihXfp
def as_binary (n):
  return '{0:015b}'.format(int(n))
 
# Convert a textual line of Hack assembly into a binary string.
# There are probably be better ways.

def convertInstruction (line):
  # We'll return this at the end.
  
  binary_string = "ERROR: %s" % line

  search_result = re.search('(.*);(.*)', line)
  if search_result != None:
    #print search_result.group(1)
    #print search_result.group(2)
    j = j_bits[search_result.group(2)]
    rhs = search_result.group(1)
    if rhs in comp1.keys():
      a = 1
      binary_string = "111%s%s%s%s" % (a, comp1[rhs], '000', j)
    else:
      a = 0
      binary_string = "111%s%s%s%s" % (a, comp0[rhs], '000', j)    
  
  search_result = re.search("@(\d+)", line)
  if search_result != None:
    binary_string = "0" + as_binary(search_result.group(1))
    
  search_result = re.search('^(.*?)=(.*?)$', line)
  if search_result != None:
    lhs = search_result.group(1)
    rhs = search_result.group(2)
    # print "L %s R %s" % (lhs, rhs)
    j ="000"
    if rhs in comp1.keys():
      a = 1
      binary_string = "111%s%s%s%s" % (a, comp1[rhs], dest[lhs], j)
    else:
      a = 0
      binary_string = "111%s%s%s%s" % (a, comp0[rhs], dest[lhs], j)      
      
  
    
  
  # Our 'if' clauses should have set the value of "binary_string"
  # before this point. If not, we will return the line that caused
  # the error for our conversion. That will help us debug our assembler.
  return binary_string
 
# Instead of doing everything in the loop, I
# put the original contents into a function.
# It does the same thing, but this allows us to extend our
# processing within the loop with new functions.
# For example, you need to build a "lookup table"
# for variables when you see things like:
# 
# @varRef
#
# and turn those into actual RAM locations. That must happen before
# we can write out binary.
variableList = []
variableDict = {}

for i in range(0, 15):
  stri = "R%s" % i
  variableDict[stri] = i

variableDict["SP"] = 0
variableDict["LCL"] = 1
variableDict["ARG"] = 2
variableDict["THIS"]= 3
variableDict["THAT"]= 4
variableDict["SCREEN"]= 16384
variableDict["KBD"]= 24576

def words(f):
    for line in f:
      line = re.sub('//.*', '',line)
      for word in line.split():
        variableList.append(word)

 

#variables
words(f)
i=16
for word in variableList:
  variable = re.search("@[A-z]", word)

  if variable != None:
    if word not in variableDict.keys():
      variableDict[word] =  i
      i+=1

#labels
i=0
for word in variableList:
  variable = re.search("\(*.\)", word)
  word = word.replace("(", "")
  word = word.replace(")", "")
  word = '@' + word
  if variable != None:
    if word not in variableDict.keys():
      variableDict[word] = i+1
    else:
      variableDict[word] = i+1
  i+=1
   

for n,i in enumerate(variableList):
  if i in variableDict.keys():
    variableList[n]= '@' + str(variableDict[i])
   

for word in variableList:
  variable = re.search("\(*.\)", word)
  if variable != None:
    variableList.remove(word)

f = open(output_file,'w')


for word in variableList:
  f.write(convertInstruction(word)+'\n') 
  
